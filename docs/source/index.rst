.. mbtrack2 documentation master file, created by
   sphinx-quickstart on Sun Apr 28 21:51:15 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
About mbtrack2
====================================

.. include:: ../../README.md
   :parser: myst_parser.sphinx_

mbtrack2 documentation
====================================

.. toctree::
   :maxdepth: 6
   :caption: Table of contents:

   mbtrack2


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

FROM gitlab-registry.synchrotron-soleil.fr/pa/collective-effects/python_mpi:latest
LABEL name mbtrack2
USER dockeruser
WORKDIR '/home/dockeruser'
ENV PATH ${PATH}:/home/dockeruser/miniconda3/bin:/home/dockeruser/miniconda3/condabin
RUN pip3 install accelerator-toolbox==0.5.0
COPY --chown=dockeruser mbtrack2 /home/dockeruser/mbtrack2
ENV PYTHONPATH=/home/dockeruser/